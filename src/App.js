import {gDevcampReact, percentStudentStudying} from "./info"

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} width="500px"/>
      <p>tỉ lệ sinh viên đang theo học là: {percentStudentStudying()} %</p>
      <p>{(percentStudentStudying() >= 15) ? "sinh viên đăng kí học nhiều" : "sinh viên đăng kí học ít"}</p>
      <ul>
        {
          gDevcampReact.benefits.map((element, index) => {
            return <li key={index}>{element}</li>
          })
          }
        
      </ul>
    </div>
  );
}

export default App;
