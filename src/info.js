import devcampImage from "./assets/images/logo-og.png";
 export const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: devcampImage,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
  }
  
  export const percentStudentStudying = () => {
    return gDevcampReact.studyingStudents / gDevcampReact.totalStudents *100;
  }